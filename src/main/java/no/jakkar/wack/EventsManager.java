package no.jakkar.wack;

import no.jakkar.wack.utilities.CommandHandler;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.events.message.react.GenericMessageReactionEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class EventsManager extends ListenerAdapter {
    CommandHandler commandHandlerObj;
    private Logger logger = LoggerFactory.getLogger(EventsManager.class);

    public EventsManager(CommandHandler commandHandlerObj) {
        this.commandHandlerObj = commandHandlerObj;
    }

    @Override
    public void onMessageReceived(MessageReceivedEvent event) {
        commandHandlerObj.checkCommand(event);
    }

    @Override
    public void onGenericMessageReaction(GenericMessageReactionEvent event) {
        logger.info("Someone reacted to a message :o");
    }
}
