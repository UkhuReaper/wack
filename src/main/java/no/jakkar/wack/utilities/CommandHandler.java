package no.jakkar.wack.utilities;

import no.jakkar.wack.commands.Command;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import org.reflections.Reflections;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Set;

public class CommandHandler {
    private static HashMap<String, Command> commands = new HashMap<String, Command>();
    private static Logger logger = LoggerFactory.getLogger(CommandHandler.class);
    private static final String prefix = "!";

    public static void genCommands() {
        Reflections reflections = new Reflections("no.jakkar.wack.commands");
        Set<Class<? extends Command>> commandClasses = reflections.getSubTypesOf(Command.class);

        for (Class<? extends Command> command : commandClasses) {
            try {
                Command commandObj = null;

                if (command.getSimpleName().equals("Help")) {
                    commandObj = command.getDeclaredConstructor(HashMap.class).newInstance(commands);
                } else {
                    commandObj = command.getDeclaredConstructor().newInstance();
                }

                commands.put(prefix + commandObj.getCommand(), commandObj);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        logger.info("Generated commands");
    }

    public void checkCommand(MessageReceivedEvent event) {
        String[] msgArray = event.getMessage().getContentDisplay().split(" ");

        if(event.getAuthor().isBot()) {
            return;
        }

        if ((msgArray.length == 1) && (commands.get(msgArray[0]) != null) && (event.getMessage().getContentDisplay().startsWith(prefix))) {
            commands.get(msgArray[0].toLowerCase()).run(event, null);
            logger.info("Executed command: " + event.getMessage().getContentDisplay());
        } else if ((msgArray.length > 1) && (event.getMessage().getContentDisplay().startsWith(prefix))) {
            try {
                commands.get(msgArray[0].toLowerCase()).run(event, msgArray);
            } catch (Exception e) {
                logger.info("Couldn't run command " + msgArray[0]); // TODO: try to find a better way to do this,
                e.printStackTrace();
            }
        }
    }
}
