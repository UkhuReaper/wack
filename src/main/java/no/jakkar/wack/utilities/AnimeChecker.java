package no.jakkar.wack.utilities;

// inspired by this but in java :-)
// https://github.com/coehler/Anime-Auditor

import org.opencv.core.*;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;
import org.opencv.objdetect.CascadeClassifier;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AnimeChecker {
  private static Logger logger = LoggerFactory.getLogger(AnimeChecker.class);


  public static boolean checkImage(String userId) {

    // load stuff
    nu.pattern.OpenCV.loadShared();
    CascadeClassifier animeFaceDetector =
        new CascadeClassifier("./lbpcascade_animeface.xml");

    // get image
    Mat image = Imgcodecs.imread("/tmp/" + userId +" .png");

    // do the cool stuff
    MatOfRect faceVectors = new MatOfRect();
    animeFaceDetector.detectMultiScale(image, faceVectors,
        // idk
        1.1, 5, 0, new Size(24,24));
    logger.info("Amount of anime found: " + faceVectors.toArray().length);

    if (faceVectors.toArray().length != 0) {
      // draw the square
      Imgproc.rectangle(image, faceVectors.toArray()[0], new Scalar(0,0,255), 5);

      // save the new image
      Imgcodecs.imwrite("//tmp/" + userId + "-marked.png", image);
      return true;
    } else {
      return false;
    }
  }

}
