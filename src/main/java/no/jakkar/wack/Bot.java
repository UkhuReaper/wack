package no.jakkar.wack;

import no.jakkar.wack.utilities.CommandHandler;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.JDABuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.security.auth.login.LoginException;

public class Bot {
    private static Logger logger = LoggerFactory.getLogger(Bot.class);

    public static void main(String[] args) {
        try {
            JDA jda = new JDABuilder(args[0]) // bot token
                    .addEventListeners(new EventsManager(new CommandHandler())) // eventListener
                    .build();
            jda.awaitReady(); // wait until bot is ready
            logger.info("Finished building JDA!");

            CommandHandler.genCommands();
        } catch (LoginException e) {
            logger.error("LoginException occurred!");
        } catch (InterruptedException e) {
            logger.error("InterruptedException occurred!");
        }
    }
}
