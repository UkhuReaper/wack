package no.jakkar.wack.commands;

import no.jakkar.wack.utilities.AnimeChecker;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;

public class AnimeCheck extends Command {

  private String command;
  private String commandDesc;

  public AnimeCheck() {
    command = "animecheck";
    commandDesc = "R u anime?";
  }

  public String getCommand() {
    return this.command;
  }

  public String getCommandDesc() {
    return this.commandDesc;
  }

  @Override
  public void run(MessageReceivedEvent event, String[] args) {

    List<User> mentionedUsers = event.getMessage().getMentionedUsers();

    // check if no users were tagged
    if (mentionedUsers.isEmpty()) {
      event.getChannel()
          .sendMessage("U gotta tag someone dummy :-)")
          .queue();
      return;
    }

    // check if user has an avatar
    User mentionedUser = mentionedUsers.get(0);
    if (mentionedUser.getAvatarUrl() == null) {
      event.getChannel()
          .sendMessage("U dumb shit " + mentionedUser.getName() + " doesn't have an avatar")
          .queue();
      return;
    }

    // do the stuff
    // TODO: do the stuff in memory and not save as file every time maybe?
    try {
      // connection time
      URLConnection urlConnection = new URL(mentionedUser.getAvatarUrl())
          .openConnection();
      // discord is mad if you don't have user agent
      urlConnection.addRequestProperty("User-Agent",
          "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36");
      urlConnection.connect();

      // get image
      BufferedImage image = ImageIO.read(urlConnection.getInputStream());
      urlConnection.getInputStream()
          .close();

      // save image as file
      File outputfile = new File("/tmp/"
          + mentionedUser.getId()
          + ".png");
      ImageIO.write(image, "png", outputfile);
    } catch (IOException e) {
      e.printStackTrace();
    }

    // check for anime
    boolean result = AnimeChecker.checkImage(mentionedUser.getId());

    if (result) {
      // announce weeb if detected
      event.getChannel()
          .sendMessage("What a weeb :^) "
              + mentionedUser.getName())
          .addFile(new File("/tmp/"
              + mentionedUser.getId()
              + "-marked.png"))
          .queue();

      // get that marked image file outta here
      File markedImageFile = new File("/tmp/"
          + mentionedUser.getId()
          + "-marked.png");
      markedImageFile.delete();
    } else {
      // be sad if no detection
      event.getChannel()
          .sendMessage(mentionedUser.getName()
              + " is not anime :-(")
          .queue();
    }

    // get that image file outta here
    File imageFile = new File("/tmp/"
        + mentionedUser.getId()
        + ".png");
    imageFile.delete();
  }
}
