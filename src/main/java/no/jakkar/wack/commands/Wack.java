package no.jakkar.wack.commands;

import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;

public class Wack extends Command{

    private String command;
    private String commandDesc;

    public Wack() {
        command = "wack";
        commandDesc = "The meme.";
    }

    public void run(MessageReceivedEvent event, String[] args) {
        EmbedBuilder eb = new EmbedBuilder();
        eb.setTitle("Wack");
        eb.setImage("https://pbs.twimg.com/media/DV8LCKdU8AAnLKw?format=png");
        event.getChannel().sendMessage(eb.build()).queue();
    }

    public String getCommand() {
        return this.command;
    }

    public String getCommandDesc() {
        return this.commandDesc;
    }
}
