package no.jakkar.wack.commands;

import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;

public class Help extends Command {
    private static Logger logger = LoggerFactory.getLogger(Help.class);

    private String command;
    private String commandDesc;
    private HashMap<String, Command> commandHashMap;

    public Help(HashMap<String, Command> commandHashMap) {
        command = "help";
        commandDesc = "Get info about different commands";
        this.commandHashMap = commandHashMap;
    }

    public String getCommand() {
        return this.command;
    }

    public String getCommandDesc() {
        return this.commandDesc;
    }

    @Override
    public void run(MessageReceivedEvent event, String[] args) {
        if (args != null) {
            try {
                EmbedBuilder eb = new EmbedBuilder();
                eb.setTitle(commandHashMap.get("!" + args[1]).getCommand());
                eb.setDescription(commandHashMap.get("!" + args[1]).getCommandDesc());
                event.getChannel().sendMessage(eb.build()).queue();
            } catch (Exception e){
                event.getChannel().sendMessage(args[1] + " is not a command!").queue();
            }
        } else {
            event.getChannel().sendMessage("You need to specify a command to get information about!").queue();
        }
    }
}
