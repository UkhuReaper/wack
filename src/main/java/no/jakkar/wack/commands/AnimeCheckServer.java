package no.jakkar.wack.commands;

import no.jakkar.wack.utilities.AnimeChecker;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

public class AnimeCheckServer extends Command {

  private String command;
  private String commandDesc;

  public AnimeCheckServer() {
    command = "animecheckserver";
    commandDesc = "noone can hide >:)";
  }

  public String getCommand() {
    return this.command;
  }

  public String getCommandDesc() {
    return this.commandDesc;
  }

  @Override
  public void run(MessageReceivedEvent event, String[] args) {

    List<User> allUsers = new ArrayList<>();
    event.getGuild().getMembers().forEach(member -> allUsers.add(member.getUser()));

    allUsers.forEach(user -> {
      // check if user has an avatar
      if (user.getAvatarUrl() == null) {
        return;
      }

      // do the stuff
      try {
        // connection time
        URLConnection urlConnection = new URL(user.getAvatarUrl())
            .openConnection();
        // discord is mad if you don't have user agent
        urlConnection.addRequestProperty("User-Agent",
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36");
        urlConnection.connect();

        // get image
        BufferedImage image = ImageIO.read(urlConnection.getInputStream());
        urlConnection.getInputStream()
            .close();

        // save image as file
        File outputfile = new File("/tmp/"
            + user.getId()
            + ".png");
        ImageIO.write(image, "png", outputfile);
      } catch (IOException e) {
        e.printStackTrace();
      }

      // check for anime
      boolean result = AnimeChecker.checkImage(user.getId());

      if (result) {
        // announce weeb if detected
        event.getChannel()
            .sendMessage("What a weeb :^) "
                + user.getName())
            .addFile(new File("/tmp/"
                + user.getId()
                + "-marked.png"))
            .queue();

        // get that marked image file outta here
        File markedImageFile = new File("/tmp/"
            + user.getId()
            + "-marked.png");
        markedImageFile.delete();
      }

      // get that image file outta here
      File imageFile = new File("/tmp/"
          + user.getId()
          + ".png");
      imageFile.delete();
    });
  }
}
