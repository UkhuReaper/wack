package no.jakkar.wack.commands;

import net.dv8tion.jda.api.entities.ChannelType;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;

import java.util.List;

public class Random extends Command {

    private String command;
    private String commandDesc;

    public Random() {
        command = "random";
        commandDesc = "Make the bot tag a random user in a guild.";
    }

    public String getCommand() {
        return this.command;
    }

    public String getCommandDesc() {
        return this.commandDesc;
    }

    public void run(MessageReceivedEvent event, String[] args) {
        if (event.isFromType(ChannelType.TEXT)) {
            Guild guild = event.getGuild();
            List<Member> allMembers = guild.getMembers();

            Member chosenMember = allMembers.get((int) (Math.random() * (((allMembers.size() - 1)) + 1)));
            event.getChannel().sendMessage("<@" + chosenMember.getId() + ">").queue();
        } else {
            event.getChannel().sendMessage("This command can only be run in servers!").queue();
        }
    }
}
