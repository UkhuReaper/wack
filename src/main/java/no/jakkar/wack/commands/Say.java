package no.jakkar.wack.commands;

import net.dv8tion.jda.api.events.message.MessageReceivedEvent;

public class Say extends Command {
    private String command;
    private String commandDesc;

    public Say() {
        this.command = "say";
        this.commandDesc = "Makes the bot say something";
    }

    public void run(MessageReceivedEvent event, String[] args) {
        StringBuilder message = new StringBuilder();
        try {
            for (int i = 1; i < args.length; i++) {
                message.append(" ").append(args[i]);
            }
            event.getChannel().sendMessage(message).queue();
            event.getMessage().delete().queue();
        }
        catch(NullPointerException e) {
            event.getChannel().sendMessage("You have to type in something after the command").queue();
        }
    }

    public String getCommand() {
        return this.command;
    }

    public String getCommandDesc() {
        return this.commandDesc;
    }
}
