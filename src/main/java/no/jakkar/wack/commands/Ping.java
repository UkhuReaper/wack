package no.jakkar.wack.commands;

import net.dv8tion.jda.api.events.message.MessageReceivedEvent;

public class Ping extends Command {

    private String command;
    private String commandDesc;

    public Ping() {
        command = "ping";
        commandDesc = "Ping pong : - )";
    }

    public String getCommand() {
        return this.command;
    }

    public String getCommandDesc() {
        return this.commandDesc;
    }

    @Override
    public void run(MessageReceivedEvent event, String[] args) {
        event.getChannel().sendMessage("<@" + event.getAuthor().getId() + "> pong!").queue();
    }
}
