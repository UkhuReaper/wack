package no.jakkar.wack.commands;

import net.dv8tion.jda.api.events.message.MessageReceivedEvent;

public class Command {

    private String command;
    private String commandDesc;

    public Command() { }

    public void run(MessageReceivedEvent event, String[] args) { }

    public String getCommand() {
        return this.command;
    }

    public String getCommandDesc() {
        return this.commandDesc;
    }
}
